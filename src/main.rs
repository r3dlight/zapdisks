use clap::{crate_version, App, Arg};
use colored::*;
use regex::Regex;
use std::fs;
use std::io;
use std::path::Path;
use std::process::Command;

/// List files in a directory except hidden ones
fn list_disks(directory: &str) -> Vec<String> {
    let paths: std::fs::ReadDir = fs::read_dir(directory).expect("Cannot read /sys/block directory !");
    let mut names = paths
        .filter_map(|entry| {
            entry.ok().and_then(|e| {
                e.path()
                    .file_name()
                    .and_then(|n| n.to_str().map(|s| String::from(s)))
            })
        })
        .collect::<Vec<String>>();
    //Not listing any files starting with loop, dm, sr, nbd
    let re = Regex::new(r"^loop|dm|sr|nb").expect("Cannot create mandatory regex !");
    names.retain(|x| !re.is_match(&x));
    names
}

fn main() {
    // Start clap CLI definition
    let matches: clap::ArgMatches<'_> = App::new("ZapDisks")
        .version(crate_version!())
        .author("r3dlight")
        .about("Zapdisks utility.")
        .arg(
            Arg::with_name("dry")
                .short("d")
                .long("dry-run")
                .value_name("true|false")
                .help("Take a bool, --run-dry=true or --run-dry=false")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("ignore")
                .short("i")
                .long("ignore")
                .value_name("$DISK")
                .help("Take a string, --ignore=sdc, default is ignoring sda. You can ignore sd* like that --ignore=sd")
                .takes_value(true),
        )
        .get_matches();
    let dry: &str = matches.value_of("dry").unwrap_or("true");
    let dry = dry
        .parse::<bool>()
        .expect("Cannot parse dry-run option to boolean ! Please use true or false value only.");
    let ignore: &str = matches.value_of("ignore").unwrap_or("sda");

    let asciiart = r"
    ______              _____   _       _         
    |___  /             |  __ \ (_)     | |        
       / /  __ _  _ __  | |  | | _  ___ | | __ ___ 
      / /  / _` || '_ \ | |  | || |/ __|| |/ // __|
     / /__| (_| || |_) || |__| || |\__ \|   < \__ \
    /_____|\__,_|| .__/ |_____/ |_||___/|_|\_\|___/
                 | |                               
                 |_|              utility v0.1.2
                                
    ";
    println!("{}", asciiart.purple());
    if dry == true {
        println!("{}", "! This is just a dry-run test !\n".green());
    } else if dry == false {
        println!(
            "{}",
            "! Warning ! This is just NOT a dry-run test, all found disks will be cleared !\n".red()
        );
    } else {
        println!("{}", "Unrecognized option for -d !\n".red());
        panic!("Unrecognized option for -d");
    }
    println!("Ignoring disks matching '{}' pattern !", ignore.red());
    println!("{}", "Gathering system information:\n".green());

    let path = Path::new("/sys/block");

    let disks = list_disks(path.to_str().expect("Cannot list disks !"));
    println!("{}", "Found the following disk list:".yellow());
    println!("{}: {:?}", "* Disks: ".yellow(), disks);
    let mut hdd = Vec::new();
    let mut ssd = Vec::new();

    for disk in disks {
        let infopath = path.join(&disk).join("queue/rotational");
        if infopath.exists() == true {
            let rotational =
                fs::read_to_string(&infopath).expect("Something went wrong reading the procnode");
            if rotational.trim().contains("1"){
                println!("{} {:?}", "* HDD Disk: ".red(), disk.clone());
            } else if rotational.trim().contains("0") {
                println!("{} {:?}", "* SSD Disk: ".bright_blue(), disk.clone());
            } else {
                println!("{}", "Error: Cannot find if disk is SSD or HDD !".red());
            }
            if rotational.trim().contains("1") {
                if disk.clone().contains(ignore) == false {
                    println!("  -> Rotational value found is: {}", rotational.trim());
                    println!(
                        "  -> Cleaning HDD disk: {} ! \n",
                        Path::new("/dev/").join(&disk.clone()).to_str().expect("Cannot create new Path /dev/$disk")
                    );
                    hdd.push(disk);
                } else if disk.contains(ignore) == true {
                    println!("  -> Rotational value found is: {}", rotational.trim());
                    println!(
                        "  -> Ignoring HDD disk: {} !\n",
                        Path::new("/dev/").join(disk).to_str().expect("Cannot create new Path /dev/$disk")
                    );
                }
                else {
                    println!("{}", "Disk provided with --ignore option was not found !".red());
                }
            } else if rotational.trim().contains("0") {
                if disk.clone().contains(ignore) == false {
                    println!("  -> Rotational value found is: {}", rotational.trim());
                    ssd.push(disk.clone());
                    println!(
                        "  -> Discarding all sectors for SSD disk: {} !\n",
                        Path::new("/dev/").join(disk).to_str().expect("Cannot create new Path /dev/$disk")
                    );
                } else if disk.contains(ignore) == true {
                    println!("  -> Rotational value found is: {}", rotational.trim());
                    println!(
                        "  -> Ignoring SSD disk: {} !\n",
                        Path::new("/dev/").join(disk).to_str().expect("Cannot create new Path /dev/$disk")
                    );
                }
                else {
                    println!("{}", "Disk provided with --ignore option was not found !".red());
                }
            } else {
                println!(
                    "ERROR: unsupported number retreived from {}",
                    infopath.to_str().expect("Cannot infopath into str")
                )
            }
        }
    }
    println!("Will clean the following HDDs: {:?}", hdd);
    println!("Will clean the following SSDs: {:?}", ssd);
    
    if dry == false {
        println!("Are you sure you want to proceed ? (yes/no)");
        let mut input = String::new();
        io::stdin().read_line(&mut input).expect("Cannot read from stdin !");
        if input.trim().contains("yes") {
            println!("{}", "Ok, zapping all disks found !".red());
            println!("HDDs: {:?}", hdd);
            for h in hdd {
                let erase_path_hdd = format!("{}{}{}", "of=","/dev/", h);
                Command::new("dd")
                    .arg("if=/dev/zero")
                    .arg(erase_path_hdd)
                    .arg("bs=1M")
                    .arg("count=100")
                    .arg("oflag=direct,dsync")
                    .spawn()
                    .expect("Error while processing command: dd if=/dev/zero of=$DISK bs=1M count=100 oflag=direct,dsync");
            }
            println!("SSDs: {:?}", ssd);
            for s in ssd {
                let erase_path_hdd = format!("{}{}", "/dev/", s);
                Command::new("blkdiscard")
                    .arg(erase_path_hdd)
                    .spawn()
                    .expect("Error whole processing command: blkdiscard $DISK");
            }
        } else {
            println!("{}", "Aborting !".green());
        }
    }
}
