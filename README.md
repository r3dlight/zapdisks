# ZapDisks

An utility to erase your SSD and HDD disks.


Help :
```bash
zapdisks -h
```

By default, zapdisks runs in "dry-run" mode which won't touch your disks at all.

To ignore /dev/sdb :
```bash
./zapdisks --ignore sdb
```

To ignore all nvme disks : 
```bash
./zapdisks -i nvme
```

To erase your disks except nvme0n1:

```bash
./zapdisks --dry-run=false -i nvme0n1
```
